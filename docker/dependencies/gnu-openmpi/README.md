# GNU-OpenMPI Dependency Container

To rebuild Docker file, use HPC container maker
```
hpccm --format=docker --recipe=gnu-openmpi-hdf5.py > Dockerfile
```
